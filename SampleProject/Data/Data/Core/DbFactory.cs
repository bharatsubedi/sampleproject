﻿using Data.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data
{
    public class DbFactory : Disposable, IDbFactory
    {
        DbEntityContext dbContext;

        public DbEntityContext Init()
        {
            return dbContext ?? (dbContext = new DbEntityContext());
        }

        protected override void DisposeCore()
        {
            if (dbContext != null)
                dbContext.Dispose();
        }
    }
}
