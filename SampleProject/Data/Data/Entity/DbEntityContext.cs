﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WorkDatabase;

namespace Data.Entity
{
    public class DbEntityContext : SampleDatabaseEntities
    {
        public virtual void Commit()
        {
            base.SaveChanges();
        }
    }
}
