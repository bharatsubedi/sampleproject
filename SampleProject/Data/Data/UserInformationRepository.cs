﻿using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data
{
    public interface IUserInformationRepository : IRepository<UserInformation>
    {
    }

    public class UserInformationRepository : RepositoryBase<UserInformation>, IUserInformationRepository
    {
        public UserInformationRepository(IDbFactory dbFactory)
            : base(dbFactory)
        {
        }
    }
}
