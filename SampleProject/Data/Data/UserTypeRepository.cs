﻿using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data
{
    public interface IUserTypeRepository : IRepository<UserType>
    {
    }

    public class UserTypeRepository : RepositoryBase<UserType>, IUserTypeRepository
    {
        public UserTypeRepository(IDbFactory dbFactory)
            : base(dbFactory)
        {
        }
    }
}
