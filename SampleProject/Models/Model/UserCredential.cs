//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Model
{
    using System;
    using System.Collections.Generic;
    
    public partial class UserCredential
    {
        public int UserCredentialId { get; set; }
        public string Email { get; set; }
        public string UserPassword { get; set; }
        public int UserInformationId { get; set; }
    
        public virtual UserInformation UserInformation { get; set; }
    }
}
