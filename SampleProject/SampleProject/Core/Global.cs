﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Security.Principal;
using System.Web;

namespace SampleProject
{
    public static class IdentityExtensions
    {
        public static int GetUserInformationId(this IIdentity identity)
        {
            var primarySid = ((ClaimsIdentity)identity).Claims
                    .Where(c => c.Type == ClaimTypes.PrimarySid)
                    .Select(c => c.Value).FirstOrDefault();

            return string.IsNullOrEmpty(primarySid) ? 0 : int.Parse(primarySid);
        }

        public static string GetRole(this IIdentity identity)
        {
            return ((ClaimsIdentity)identity).Claims
                    .Where(c => c.Type == ClaimTypes.Role)
                    .Select(c => c.Value).FirstOrDefault();
        }

        public static string GetUserFullName(this IIdentity identity)
        {
            return ((ClaimsIdentity)identity).Claims
                    .Where(c => c.Type == ClaimTypes.Actor)
                    .Select(c => c.Value).FirstOrDefault();
        }
    }
}