﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace SampleProject.Api
{
    public class CommonController : BaseApiController
    {
        [HttpGet]
        public object GetUserId()
        {
            return User.Identity.GetUserInformationId();
        }
    }
}
