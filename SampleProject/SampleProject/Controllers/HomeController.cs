﻿using Microsoft.AspNet.Identity;
using Microsoft.Owin.Security;
using Model;
using SampleProject.Core;
using SampleProject.Models;
using Service;
using Service.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Web;
using System.Web.Mvc;

namespace SampleProject.Controllers
{
    public class HomeController : BaseController
    {
        private IAuthenticationManager AuthenticationManager
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }
        }

        [AllowAnonymous]
        public ActionResult Index()
        {
            return View();
        }

        [AllowAnonymous]
        public ActionResult Login(string returnUrl)
        {
            if (User.Identity.IsAuthenticated && User.Identity.GetUserName() != string.Empty)
            {
                return RedirectToLocal(string.Empty);
            }

            ViewBag.ReturnUrl = returnUrl;
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult Login(UserLoginViewModel model, string returnUrl)
        {
            if (ModelState.IsValid)
            {
                IUserCredentialService service = GetUserCredentialService();
                var user = service.Get(model.Email);
                if (user != null)
                {
                    if (StringCipher.Decrypt(user.UserPassword) != model.Password)
                    {
                        ModelState.AddModelError("", "Invalid username or password.");
                    }
                    else
                    {
                        SignIn(user.UserInformation, model.RememberMe);
                        return RedirectToLocal(returnUrl);
                    }
                }
                else
                {
                    ModelState.AddModelError("", "Invalid username or password.");
                }

            }
            return View(model);
        }

        [AllowAnonymous]
        public ActionResult Register()
        {
            ViewBag.UserTypes = GetUserTypes();
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [AllowAnonymous]
        public ActionResult Register(UserRegisterViewModel model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    var userCredential = new List<UserCredential>();
                    userCredential.Add(new UserCredential()
                    {
                        Email = model.Email,
                        UserPassword = StringCipher.Encrypt(model.Password)
                    });

                    var userInformation = new UserInformation()
                    {
                        FirstName = model.FirstName,
                        LastName = model.LastName,
                        Email = model.Email,
                        UserTypeId = model.UserTypeId,
                        UserCredentials = userCredential
                    };

                    IUserInformationService service = GetUserInformationService();
                    service.Add(userInformation);
                    service.Save();

                    var credentialInfo = GetUserCredentialService();
                    var info = credentialInfo.Get(model.Email);
                    SignIn(info.UserInformation, false);

                    return RedirectToLocal(string.Empty);

                }
                catch (System.Data.Entity.Infrastructure.DbUpdateException ex)
                {
                    var sqlException = ex.GetBaseException() as System.Data.SqlClient.SqlException;
                    if (sqlException != null)
                    {
                        if (sqlException.Errors.Count > 0)
                        {
                            switch (sqlException.Errors[0].Number)
                            {
                                case 2627:  // Unique constraint error
                                case 2601: // Duplicated key row error
                                    ModelState.AddModelError("", "Email already exists.");
                                    break;
                                default:
                                    throw;
                            }
                        }
                    }
                    else
                    {
                        throw;
                    }
                }
                catch (Exception)
                {
                    throw;
                }
            }

            ViewBag.UserTypes = GetUserTypes();
            return View(model);
        }

        public ActionResult ChangePassword()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ChangePassword(PasswordChangeViewModel model)
        {
            if (ModelState.IsValid)
            {
                IUserCredentialService service = GetUserCredentialService();
                var user = service.Get(User.Identity.GetUserName());

                var oldDecryptPassword = StringCipher.Decrypt(user.UserPassword);
                if (oldDecryptPassword != model.OldPassword)
                {
                    ModelState.AddModelError("", "Old password do not match.");
                }
                else
                {
                    var newEncryptedpassword = StringCipher.Encrypt(model.NewPassword);
                    user.UserPassword = newEncryptedpassword;

                    service.UpdateUserCredential(user);
                    service.Save();
                    return RedirectToLocal(string.Empty);                    
                }
            }
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult LogOff()
        {
            AuthenticationManager.SignOut();
            return RedirectToAction("Login", "Home");
        }

        [AllowAnonymous]
        public ActionResult AccessDenied()
        {
            return View();
        }

        private ActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
        }

        private void SignIn(UserInformation user, bool isPersistent)
        {
            var identity = new[]{ 
              // adding following 2 claim just for supporting default antiforgery provider
              new Claim(ClaimTypes.NameIdentifier, user.Email),
              new Claim("http://schemas.microsoft.com/accesscontrolservice/2010/07/claims/identityprovider", "ASP.NET Identity", "http://www.w3.org/2001/XMLSchema#string"),
              new Claim(ClaimTypes.Name, user.Email),
              new Claim(ClaimTypes.Actor, user.FirstName.Trim() + " " + user.LastName.Trim()),
              new Claim(ClaimTypes.PrimarySid, user.UserInformationId.ToString()),
              new Claim(ClaimTypes.Role, user.UserType.TypeName)
          };
            var claimIdentity = new ClaimsIdentity(identity, DefaultAuthenticationTypes.ApplicationCookie);

            AuthenticationManager.SignOut(DefaultAuthenticationTypes.ExternalCookie);
            AuthenticationManager.SignIn(new AuthenticationProperties { IsPersistent = isPersistent }, claimIdentity);

        }

        private IUserCredentialService GetUserCredentialService()
        {
            Data.IDbFactory dbFactory = new Data.DbFactory();
            Data.IUnitOfWork iUnitOfWork = new Data.UnitOfWork(dbFactory);
            Data.IUserCredentialRepository iUserCredentialRepository = new Data.UserCredentialRepository(dbFactory);
            return new UserCredentialService(iUserCredentialRepository, iUnitOfWork);
        }

        private IUserTypeService GetUserTypeService()
        {
            Data.IDbFactory dbFactory = new Data.DbFactory();
            Data.IUserTypeRepository iUserCredentialRepository = new Data.UserTypeRepository(dbFactory);
            return new UserTypeService(iUserCredentialRepository);
        }

        private IUserInformationService GetUserInformationService()
        {
            Data.IDbFactory dbFactory = new Data.DbFactory();
            Data.IUnitOfWork iUnitOfWork = new Data.UnitOfWork(dbFactory);
            Data.IUserInformationRepository iUserCredentialRepository = new Data.UserInformationRepository(dbFactory);
            return new UserInformationService(iUserCredentialRepository, iUnitOfWork);
        }


        private List<UserType> GetUserTypes()
        {
            IUserTypeService service = GetUserTypeService();
            return service.Get();
        }
    }
}