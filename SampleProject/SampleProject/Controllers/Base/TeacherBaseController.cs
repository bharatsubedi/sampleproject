﻿using SampleProject.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SampleProject.Controllers
{
    [AuthorizeRoles(Roles = "Teacher")]
    public class TeacherBaseController : BaseController
    {
        
	}
}