﻿using SampleProject.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SampleProject.Controllers
{
    public class ActionTaskController : BaseController
    {
        //
        // GET: /ActionTask/
        [AuthorizeRoles(Roles = "Administrator")]
        public ActionResult AdministratorAction()
        {
            return View();
        }

        [AuthorizeRoles(Roles = "Teacher")]
        public ActionResult TeacherAction()
        {
            return View();
        }
	}
}