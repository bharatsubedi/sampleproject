﻿using Data;
using Model;
using Service.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Service
{
    public class UserInformationService : BaseService, IUserInformationService
    {
        private readonly IUserInformationRepository userInformationRepository;
        private readonly IUnitOfWork unitOfWork;

        public UserInformationService(IUserInformationRepository userInformationRepository, IUnitOfWork unitOfWork)
        {
            this.userInformationRepository = userInformationRepository;
            this.unitOfWork = unitOfWork;
        }

        public void Add(UserInformation userInformation)
        {
            userInformationRepository.Add(userInformation);
        }

        public void Save()
        {
            unitOfWork.Commit();
        }
    }
}
