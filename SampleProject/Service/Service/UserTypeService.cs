﻿using Data;
using Model;
using Service.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Service
{
    public class UserTypeService : BaseService, IUserTypeService
    {
        private readonly IUserTypeRepository userTypeRepository;

        public UserTypeService(IUserTypeRepository userTypeRepository)
        {
            this.userTypeRepository = userTypeRepository;
        }

        public List<UserType> Get()
        {
            return userTypeRepository.GetAll().ToList();
        }
    }
}
