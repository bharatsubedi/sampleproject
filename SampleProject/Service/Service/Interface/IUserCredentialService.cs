﻿using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Service.Interface
{
    public interface IUserCredentialService
    {
        UserCredential Get(string email);

        void UpdateUserCredential(UserCredential userCredential);

        void Save();
    }
}
