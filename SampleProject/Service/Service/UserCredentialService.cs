﻿using Data;
using Model;
using Service.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Service
{
    public class UserCredentialService : BaseService, IUserCredentialService
    {
        private readonly IUserCredentialRepository userCredentialRepository;

        private readonly IUnitOfWork unitOfWork;

        public UserCredentialService(IUserCredentialRepository userCredentialRepository, IUnitOfWork unitOfWork)
        {
            this.userCredentialRepository = userCredentialRepository;
            this.unitOfWork = unitOfWork;
        }

        public UserCredential Get(string email)
        {
            return userCredentialRepository.Get(x => x.Email.Equals(email));
        }


        public void UpdateUserCredential(UserCredential userCredential)
        {
            userCredentialRepository.Update(userCredential);
        }

        public void Save()
        {
            unitOfWork.Commit();
        }
    }
}
